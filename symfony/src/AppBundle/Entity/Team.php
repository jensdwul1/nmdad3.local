<?php

namespace AppBundle\Entity;

use AppBundle\Repository\PostRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity Team.
 *
 * @ORM\Table(name="teams")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TeamRepository")
 * @ORM\HasLifecycleCallbacks
 */

class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255)
     */
    private $icon;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    // Member Variables for Relationships.
    // -----------------------------------

    /**
     * @var ArrayCollection
     *
     *
     * @ORM\OneToMany(targetEntity="Member", mappedBy="team")
     */
    private $members;

    /**
     * @var ArrayCollection
     *
     *
     * @ORM\OneToMany(targetEntity="Conversation", mappedBy="team")
     */
    private $conversations;

    /**
     * @var ArrayCollection
     *
     *
     * @ORM\OneToMany(targetEntity="Project", mappedBy="team")
     */
    private $projects;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Title", mappedBy="team")
     */
    private $titles;

    /**
     * @var Company
     *
     * Many-to-one relationship with Company inversed by Company::$teams.
     * Further reading: http://docs.doctrine-project.org/en/latest/reference/association-mapping.html#one-to-many-bidirectional
     *
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="teams")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $company;


    /**
     * @var TimerSetting
     *
     * @ORM\OneToOne(targetEntity="TimerSetting", mappedBy="team")
     */
    private $timerSetting;


    // Member Methods
    // ==============

    /**
     * Team constructor.
     */
    public function __construct()
    {
        $datetime = new DateTime();
        $this->setCreatedAt($datetime);
        $this->setMembers(new ArrayCollection());
        $this->setConversations(new ArrayCollection());
        $this->setProjects(new ArrayCollection());
        $this->setTitles(new ArrayCollection());
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return Team
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Team
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Team
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Team
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    // Member Methods for Relationships.
    // ---------------------------------

    /**
     * @return ArrayCollection
     */
    public function getMembers() //: ArrayCollection | PersistentCollection
    {
        return $this->members;
    }

    /**
     * @param ArrayCollection $members
     *
     * @return User
     */
    public function setMembers(ArrayCollection $members) : self
    {
        $this->members = $members;

        return $this;
    }
    
    /**
     * @return ArrayCollection
     */
    public function getConversations() //: ArrayCollection | PersistentCollection
    {
        return $this->conversations;
    }

    /**
     * @param ArrayCollection $conversations
     *
     * @return Team
     */
    public function setConversations(ArrayCollection $conversations) : self
    {
        $this->conversations = $conversations;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * @param ArrayCollection $projects
     * @return Team
     */
    public function setProjects($projects)
    {
        $this->projects = $projects;
        return $this;
    }
    
    /**
     * @return ArrayCollection
     */
    public function getTitles()
    {
        return $this->titles;
    }

    /**
     * @param ArrayCollection $titles
     * @return Team
     */
    public function setTitles($titles)
    {
        $this->titles = $titles;
        return $this;
    }

    /**
     * @return TimerSetting
     */
    public function getTimerSetting()
    {
        return $this->timerSetting;
    }

    /**
     * @param TimerSetting $timerSetting
     * @return Team
     */
    public function setTimerSetting($timerSetting)
    {
        $this->timerSetting = $timerSetting;
        return $this;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     * @return Team
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}


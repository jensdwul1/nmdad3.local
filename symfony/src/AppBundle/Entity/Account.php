<?php

namespace AppBundle\Entity;

use AppBundle\Repository\AccountRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use JMS\Serializer\Annotation as JMS;
use Serializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Account
 *
 * @ORM\Table(name="accounts")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccountRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Account extends UserAbstract implements AdvancedUserInterface
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="enabled_at", type="datetime", nullable=true)
     */
    private $enabledAt = null;

    /**
     * @var string
     *
     * @ORM\Column(name="picture", type="string", length=255, nullable=true)
     */
    private $account_picture;

    // Member Variables for Relationships.
    // -----------------------------------

    /**
     * @var ArrayCollection
     *
     *
     * @ORM\OneToMany(targetEntity="Member", mappedBy="account")
     */
    private $members;
    /**
     * @var ArrayCollection
     *
     *
     * @ORM\OneToMany(targetEntity="Picture", mappedBy="account")
     */
    private $pictures;


    // Member Methods
    // ==============

    /**
     * Account constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $datetime = new DateTime();
        $this->setEnabledAt($datetime);
        $this->setMembers(new ArrayCollection());
        $this->setPictures(new ArrayCollection());
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * {@inheritdoc}
     */
    public function isEnabled() : bool
    {
        return true;
    }


    /**
     * @return string
     */
    public function getAccountPicture()
    {
        return $this->account_picture;
    }

    /**
     * @param string $account_picture
     * @return Account
     */
    public function setAccountPicture($account_picture)
    {
        $this->account_picture = $account_picture;
        return $this;
    }


    /**
     * @return ArrayCollection
     */
    public function getMembers() //: ArrayCollection | PersistentCollection
    {
        return $this->members;
    }

    /**
     * @param ArrayCollection $members
     *
     * @return Account
     */
    public function setMembers(ArrayCollection $members) : self
    {
        $this->members = $members;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPictures()
    {
        return $this->pictures;
    }

    /**
     * @param ArrayCollection $pictures
     */
    public function setPictures($pictures)
    {
        $this->pictures = $pictures;
    }


    /**
     * @return DateTime|null
     */
    public function getEnabledAt()
    {
        return $this->enabledAt;
    }

    /**
     * @param DateTime|null $enabledAt
     */
    public function setEnabledAt($enabledAt)
    {
        $this->enabledAt = $enabledAt;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getFirstName().' '.$this->getLastName();
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}

<?php

namespace AppBundle\Entity;

use AppBundle\Repository\PostRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Member
 *
 * @ORM\Table(name="members")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MemberRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Member
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_leader", type="boolean")
     */
    private $isLeader;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_enabled", type="boolean")
     */
    private $isEnabled;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;
    
    /**
     * @var Team
     *
     * Many-to-one relationship with Team inversed by Team::$members.
     * Further reading: http://docs.doctrine-project.org/en/latest/reference/association-mapping.html#one-to-many-bidirectional
     *
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="members")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $team;

    /**
     * @var Title|null
     *
     * Many-to-one relationship with Title inversed by Title::$members.
     * Further reading: http://docs.doctrine-project.org/en/latest/reference/association-mapping.html#one-to-many-bidirectional
     *
     * @ORM\ManyToOne(targetEntity="Title", inversedBy="members")
     * @ORM\JoinColumn(name="title_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $title = null;
    
    /**
     * @var Account
     *
     * Many-to-one relationship with Account inversed by Account::$members.
     * Further reading: http://docs.doctrine-project.org/en/latest/reference/association-mapping.html#one-to-many-bidirectional
     *
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="members")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $account;

    /**
     * @ORM\ManyToMany(targetEntity="Issue", inversedBy="members")
     * @ORM\JoinTable(name="member_has_issues",
     *     joinColumns={@ORM\JoinColumn(name="member_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="issue_id", referencedColumnName="id")}
     * )
     */
    private $issues;

    /**
     * @ORM\ManyToMany(targetEntity="Project", inversedBy="members")
     * @ORM\JoinTable(name="member_has_projects",
     *     joinColumns={@ORM\JoinColumn(name="member_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="project_id", referencedColumnName="id")}
     * )
     */
    private $projects;


    /**
     * @var ArrayCollection
     *
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="author")
     */
    private  $sentMessages;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Conversation", mappedBy="members")
     */
    private  $conversations;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Task", mappedBy="members")
     */
    private $tasks;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Pomo", mappedBy="members")
     */
    private $pomos;



    // Member Methods
    // ==============

    /**
     * Member constructor.
     */
    public function __construct()
    {
        $datetime = new DateTime();
        $this->setCreatedAt($datetime);
        $this->setIsActive(false);
        $this->setIsEnabled(false);
        $this->setIsLeader(false);
        $this->setTasks(new ArrayCollection());
        $this->setIssues(new ArrayCollection());
        $this->setConversations(new ArrayCollection());
        $this->setSentMessages(new ArrayCollection());
        $this->setPomos(new ArrayCollection());
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isLeader
     *
     * @param boolean $isLeader
     *
     * @return Member
     */
    public function setIsLeader($isLeader)
    {
        $this->isLeader = $isLeader;

        return $this;
    }

    /**
     * Get isLeader
     *
     * @return bool
     */
    public function getIsLeader()
    {
        return $this->isLeader;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     *
     * @return Member
     */
     public function setIsEnabled($isEnabled)
     {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return bool
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }
    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Member
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Member
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Member
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Member
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    /**
     * @return ArrayCollection
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param ArrayCollection $tasks
     * @return Member
     */
    public function setTasks($tasks)
    {
        $this->tasks = $tasks;
        return $this;
    }

    /**
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param Team $team
     * @return Member
     */
    public function setTeam($team)
    {
        $this->team = $team;
        return $this;
    }
    
    /**
     * @return Title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param Title $title
     * @return Member
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return Member
     */
    public function setAccount($account)
    {
        $this->account = $account;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIssues()
    {
        return $this->issues;
    }

    /**
     * @param mixed $issues
     * @return Member
     */
    public function setIssues($issues)
    {
        $this->issues = $issues;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getSentMessages()
    {
        return $this->sentMessages;
    }

    /**
     * @param ArrayCollection $sentMessages
     * @return Member
     */
    public function setSentMessages($sentMessages)
    {
        $this->sentMessages = $sentMessages;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getConversations()
    {
        return $this->conversations;
    }

    /**
     * @param ArrayCollection $conversations
     * @return Member
     */
    public function setConversations($conversations)
    {
        $this->conversations = $conversations;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPomos()
    {
        return $this->pomos;
    }

    /**
     * @param ArrayCollection $pomos
     * @return Member
     */
    public function setPomos($pomos)
    {
        $this->pomos = $pomos;
        return $this;
    }


    /**
     * @return ArrayCollection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * @param ArrayCollection $projects
     * @return Member
     */
    public function setProjects($projects)
    {
        $this->projects = $projects;
        return $this;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTeam()->getName().' - '.$this->getTitle().' - '.$this->getAccount()->getFirstName().' - '.$this->getAccount()->getLastName();
    }


    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}


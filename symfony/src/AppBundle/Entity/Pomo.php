<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use DateTime;

/**
 * Pomo
 *
 * @ORM\Table(name="pomos")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PomoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Pomo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="estimate_time", type="string", length=40)
     */
    private $estimateTime;

    /**
     * @var string
     *
     * @ORM\Column(name="actual_time", type="string", length=40, nullable=true)
     */
    private $actualTime;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_completed", type="boolean")
     */
    private $isCompleted;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="completion_time", type="datetime", nullable=true)
     */
    private $completionTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Task", mappedBy="pomos")
     */
    private $tasks;

    /**
     * @var Project
     *
     * Many-to-one relationship with Project inversed by Project::$pomos.
     * Further reading: http://docs.doctrine-project.org/en/latest/reference/association-mapping.html#one-to-many-bidirectional
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="pomos")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $project;


    /**
     * @ORM\ManyToMany(targetEntity="Member", inversedBy="pomos")
     * @ORM\JoinTable(name="member_has_pomos",
     *     joinColumns={@ORM\JoinColumn(name="member_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="pomo_id", referencedColumnName="id")}
     * )
     */
    private $members;


    /**
     * Pomo constructor.
     */
    public function __construct()
    {
        $datetime = new DateTime();
        $this->setCreatedAt($datetime);
        $this->setTasks(new ArrayCollection());
        $this->setMembers(new ArrayCollection());
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Pomo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Pomo
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return string
     */
    public function getEstimateTime()
    {
        return $this->estimateTime;
    }

    /**
     * @param string $estimateTime
     *
     * @return Pomo
     */
    public function setEstimateTime($estimateTime)
    {
        $this->estimateTime = $estimateTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getActualTime()
    {
        return $this->actualTime;
    }

    /**
     * @param string $actualTime
     *
     * @return Pomo
     */
    public function setActualTime($actualTime)
    {
        $this->actualTime = $actualTime;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsCompleted()
    {
        return $this->isCompleted;
    }

    /**
     * @param boolean $isCompleted
     *
     * @return Pomo
     */
    public function setIsCompleted($isCompleted)
    {
        $this->isCompleted = $isCompleted;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCompletionTime()
    {
        return $this->completionTime;
    }

    /**
     * @param DateTime $completionTime
     *
     * @return Pomo
     */
    public function setCompletionTime($completionTime)
    {
        $this->completionTime = $completionTime;

        return $this;
    }

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     *
     * @return Pomo
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() : DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Pomo
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Pomo
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get Tasks
     *
     * @return ArrayCollection
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * Set Tasks
     *
     * @param ArrayCollection $tasks
     * @return Pomo
     */
    public function setTasks($tasks)
    {
        $this->tasks = $tasks;
        return $this;
    }


    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return Pomo
     */
    public function setProject($project)
    {
        $this->project = $project;
        return $this;
    }

    /**
     * Add member.
     *
     * @param Member $member
     *
     * @return Pomo
     */
    public function addMember(Member $member) : self
    {
        $this->members->add($member);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMembers() //: ArrayCollection | PersistentCollection
    {
        return $this->members;
    }

    /**
     * @param ArrayCollection $members
     *
     * @return User
     */

    public function setMembers(ArrayCollection $members) : self
    {
        $this->members = $members;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}


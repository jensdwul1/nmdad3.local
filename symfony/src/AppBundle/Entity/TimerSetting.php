<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Integer;

/**
 * TimerSettings
 *
 * @ORM\Table(name="timer_settings")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TimerSettingsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class TimerSetting
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="default_pomo", type="integer")
     */
    private $defaultPomo;

    /**
     * @var int
     *
     * @ORM\Column(name="short_break", type="integer")
     */
    private $shortBreak;

    /**
     * @var int
     *
     * @ORM\Column(name="long_break", type="integer")
     */
    private $longBreak;

    /**
     * @var int
     *
     * @ORM\Column(name="clean_up", type="integer")
     */
    private $cleanUp;

    /**
     * @var Team
     *
     * @ORM\OneToOne(targetEntity="Team", inversedBy="timerSetting")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    private $team;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set defaultPomo
     *
     * @param integer $defaultPomo
     *
     * @return TimerSettings
     */
    public function setDefaultPomo($defaultPomo)
    {
        $this->defaultPomo = $defaultPomo;

        return $this;
    }

    /**
     * Get defaultPomo
     *
     * @return Integer
     */
    public function getDefaultPomo()
    {
        return $this->defaultPomo;
    }

    /**
     * Set shortBreak
     *
     * @param \Integer $shortBreak
     *
     * @return TimerSettings
     */
    public function setShortBreak($shortBreak)
    {
        $this->shortBreak = $shortBreak;

        return $this;
    }

    /**
     * Get shortBreak
     *
     * @return \Integer
     */
    public function getShortBreak()
    {
        return $this->shortBreak;
    }

    /**
     * Set longBreak
     *
     * @param \Integer $longBreak
     *
     * @return TimerSettings
     */
    public function setLongBreak($longBreak)
    {
        $this->longBreak = $longBreak;

        return $this;
    }

    /**
     * Get longBreak
     *
     * @return \Integer
     */
    public function getLongBreak()
    {
        return $this->longBreak;
    }

    /**
     * Set cleanUp
     *
     * @param \Integer $cleanUp
     *
     * @return TimerSettings
     */
    public function setCleanUp($cleanUp)
    {
        $this->cleanUp = $cleanUp;

        return $this;
    }

    /**
     * Get cleanUp
     *
     * @return \Integer
     */
    public function getCleanUp()
    {
        return $this->cleanUp;
    }

    /**
     * @return mixed
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param mixed $team
     * @return TimerSetting
     */
    public function setTeam($team)
    {
        $this->team = $team;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '';
    }
}


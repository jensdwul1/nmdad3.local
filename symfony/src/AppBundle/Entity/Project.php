<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use DateTime;

/**
 * Projects
 *
 * @ORM\Table(name="projects")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Project
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_completed", type="boolean")
     */
    private $isCompleted;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_ongoing", type="boolean")
     */
    private $isOngoing;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_paused", type="boolean")
     */
    private $isPaused;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;


    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Member", mappedBy="projects")
     */
    private $members;


    /**
     * @var ArrayCollection
     *
     *
     * @ORM\OneToMany(targetEntity="Pomo", mappedBy="project")
     */
    private $pomos;
    
    /**
     * @var ArrayCollection
     *
     *
     * @ORM\OneToMany(targetEntity="Task", mappedBy="project")
     */
    private $tasks;

    /**
     * @var Team
     *
     * Many-to-one relationship with User inversed by User::$members.
     * Further reading: http://docs.doctrine-project.org/en/latest/reference/association-mapping.html#one-to-many-bidirectional
     *
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="projects")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $team;


    /**
     * @var ArrayCollection
     *
     *
     * @ORM\OneToMany(targetEntity="Issue", mappedBy="project")
     */
    private $issues;

    /**
     * Task constructor.
     */
    public function __construct()
    {
        $datetime = new DateTime();
        $this->setCreatedAt($datetime);
        $this->setMembers(new ArrayCollection());
        $this->setPomos(new ArrayCollection());
        $this->setTasks(new ArrayCollection());
        $this->setIsCompleted(false);
        $this->setIsOngoing(false);
        $this->setIsPaused(false);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isCompleted
     *
     * @param boolean $isCompleted
     *
     * @return Project
     */
    public function setIsCompleted($isCompleted)
    {
        $this->isCompleted = $isCompleted;

        return $this;
    }

    /**
     * Get isCompleted
     *
     * @return bool
     */
    public function getIsCompleted()
    {
        return $this->isCompleted;
    }

    /**
     * Set isOngoing
     *
     * @param boolean $isOngoing
     *
     * @return Project
     */
    public function setIsOngoing($isOngoing)
    {
        $this->isOngoing = $isOngoing;

        return $this;
    }

    /**
     * Get isOngoing
     *
     * @return bool
     */
    public function getIsOngoing()
    {
        return $this->isOngoing;
    }

    /**
     * Set isPaused
     *
     * @param boolean $isPaused
     *
     * @return Project
     */
    public function setIsPaused($isPaused)
    {
        $this->isPaused = $isPaused;

        return $this;
    }

    /**
     * Get isPaused
     *
     * @return bool
     */
    public function getIsPaused()
    {
        return $this->isPaused;
    }

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     *
     * @return Project
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return DateTime
     */
    public function getCreatedAt() : DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return Project
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param DateTime $deletedAt
     *
     * @return Project
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Get members.
     *
     * @return ArrayCollection
     */

    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Set members.
     *
     * @param ArrayCollection $members
     *
     * @return Project
     */
    public function setMembers(ArrayCollection $members) : self
    {
        $this->members = $members;
        return $this;
    }

    /**
     * Add pomos.
     *
     * @param Message $pomos
     *
     * @return Project
     */
    public function addProject(Pomo $pomos) : self
    {
        $this->pomos->add($pomos);
        return $this;
    }
    /**
     * Get pomos.
     *
     * @return ArrayCollection
     */
    public function getPomos()
    {
        return $this->pomos;
    }
    /**
     * Set pomos.
     *
     * @param ArrayCollection $pomos
     *
     * @return Project
     */
    public function setPomos(ArrayCollection $pomos) : self
    {
        $this->pomos = $pomos;
        return $this;
    }
    /**
     * Get tasks.
     *
     * @return ArrayCollection
     */
    public function getTasks()
    {
        return $this->tasks;
    }
    /**
     * Set tasks.
     *
     * @param ArrayCollection $tasks
     *
     * @return Project
     */
    public function setTasks(ArrayCollection $tasks) : self
    {
        $this->tasks = $tasks;
        return $this;
    }

    /**
     * Get issues.
     *
     * @return ArrayCollection
     */
    public function getIssues()
    {
        return $this->issues;
    }
    /**
     * Set issues.
     *
     * @param ArrayCollection $issues
     *
     * @return Project
     */
    public function setIssues(ArrayCollection $issues) : self
    {
        $this->issues = $issues;
        return $this;
    }

    /**
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param Team $team
     * @return Project
     */
    public function setTeam($team)
    {
        $this->team = $team;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}


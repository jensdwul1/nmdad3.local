<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use DateTime;

/**
 * Task
 *
 * @ORM\Table(name="tasks")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Task
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="finished", type="boolean")
     */
    private $finished;


    /**
     * @var string
     *
     * @ORM\Column(name="estimate_time", type="string", length=40)
     */
    private $estimateTime;

    /**
     * @var string
     *
     * @ORM\Column(name="actual_time", type="string", length=40, nullable=true)
     */
    private $actualTime;


    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;


    /**
     * @ORM\ManyToMany(targetEntity="Member", inversedBy="tasks")
     * @ORM\JoinTable(name="member_has_tasks",
     *     joinColumns={@ORM\JoinColumn(name="member_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="task_id", referencedColumnName="id")}
     * )
     */
    private $members;

    /**
     * @ORM\ManyToMany(targetEntity="Pomo", inversedBy="tasks")
     * @ORM\JoinTable(name="pomo_has_tasks",
     *     joinColumns={@ORM\JoinColumn(name="pomo_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="task_id", referencedColumnName="id")}
     * )
     */
    protected $pomos;

    /**
     * @var Project
     *
     * Many-to-one relationship with Project inversed by Project::$tasks.
     * Further reading: http://docs.doctrine-project.org/en/latest/reference/association-mapping.html#one-to-many-bidirectional
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="tasks")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $project;




    // Member Methods
    // ==============
    
    
    /**
     * Task constructor.
     */
    public function __construct()
    {
        $datetime = new DateTime();
        $this->setPomos(new ArrayCollection());
        $this->setMembers(new ArrayCollection());
        $this->setCreatedAt($datetime);
    }
    
    
    
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Task
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Task
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set finished
     *
     * @param boolean $finished
     *
     * @return Task
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;

        return $this;
    }

    /**
     * Get finished
     *
     * @return bool
     */
    public function getFinished()
    {
        return $this->finished;
    }


    /**
     * @return string
     */
    public function getEstimateTime()
    {
        return $this->estimateTime;
    }

    /**
     * @param string $estimateTime
     */
    public function setEstimateTime($estimateTime)
    {
        $this->estimateTime = $estimateTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getActualTime()
    {
        return $this->actualTime;
    }

    /**
     * @param string $actualTime
     */
    public function setActualTime($actualTime)
    {
        $this->actualTime = $actualTime;

        return $this;
    }

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     *
     * @return Task
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return DateTime
     */
    public function getCreatedAt() : DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return Task
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param DateTime $deletedAt
     *
     * @return Task
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }


    /**
     * Add member.
     *
     * @param Member $member
     *
     * @return Task
     */
    public function addMember(Member $member) : self
    {
        $this->members->add($member);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMembers() //: ArrayCollection | PersistentCollection
    {
        return $this->members;
    }

    /**
     * @param ArrayCollection $members
     *
     * @return User
     */

    public function setMembers(ArrayCollection $members) : self
    {
        $this->members = $members;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPomos()
    {
        return $this->pomos;
    }

    /**
     * @param ArrayCollection $pomos
     * @return Member
     */
    public function setPomos($pomos)
    {
        $this->pomos = $pomos;
        return $this;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}


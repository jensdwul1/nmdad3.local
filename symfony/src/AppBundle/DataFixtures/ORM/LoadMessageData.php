<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Message;
use AppBundle\Entity\Conversation;
use AppBundle\Entity\Member;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadMessageData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 */
class LoadMessageData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerTrait, PasswordTrait;

    const COUNT = 100;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 13; // The order in which fixture(s) will be loaded.
    }

    public function load(ObjectManager $em)
    {
        $logger = $this->getContainer()->get('logger');

        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $allConversations = $em->getRepository(Conversation::class)->findAll();

        // Fake messages.
        for ($messageCount = 0; $messageCount < self::COUNT; ++$messageCount) {
            $message = new Message();
            $em->persist($message); // Manage Entity for persistence.
            $conversation = $faker->randomElement($allConversations);
            $allMembers = $conversation->getMembers();
            $allMembers = $allMembers->toArray();
            //$allMembers = $em->getRepository(Member::class)->findByConversations($conversation);
            $author = $faker->randomElement($allMembers);

            $message
                ->setContent($faker->paragraph())
                ->setConversation($conversation)
                ->setAuthor($author)
                ->setCreatedAt($faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now'));
            $this->addReference("TestMessage-${messageCount}", $message); // Reference for the next Data Fixture(s).

        }

        $em->flush(); // Persist all managed Entities.

    }
}

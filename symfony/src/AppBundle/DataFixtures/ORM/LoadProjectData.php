<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Project;
use AppBundle\Entity\Team;
use AppBundle\Entity\Member;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadProjectData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 */
class LoadProjectData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerTrait, PasswordTrait;

    const COUNT = 40;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 6; // The order in which fixture(s) will be loaded.
    }

    public function load(ObjectManager $em)
    {

        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $allTeams = $em->getRepository(Team::class)->findAll();



        foreach (range(1, 3) as $i) {
            // Fake projects.
            foreach($allTeams as $key => $team){
                $project = new Project();
                $em->persist($project); // Manage Entity for persistence.
                $allMembers = $em->getRepository(Member::class)->findByTeam($team);
                $members = new ArrayCollection($faker->randomElements($allMembers, $count = $faker->numberBetween($min = 1, $max = 3)));

                $project
                    ->setName($faker->word())
                    ->setDescription($faker->paragraph())
                    ->setIsCompleted(0)
                    ->setIsOngoing(0)
                    ->setIsPaused(0)
                    ->setTeam($team)
                    ->setMembers($members)
                    ->setCreatedAt($faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now'));
                $this->addReference("TestProject-${i}${key}", $project); // Reference for the next Data Fixture(s).

            }
        }

        $em->flush(); // Persist all managed Entities.

    }
}

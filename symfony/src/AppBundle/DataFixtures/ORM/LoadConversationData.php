<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Conversation;
use AppBundle\Entity\Team;
use AppBundle\Entity\Member;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadConversationData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 */
class LoadConversationData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerTrait, PasswordTrait;

    const COUNT = 30;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 12; // The order in which fixture(s) will be loaded.
    }

    public function load(ObjectManager $em)
    {
        $logger = $this->getContainer()->get('logger');

        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $allTeams = $em->getRepository(Team::class)->findAll();

        // Fake conversations.
        for ($conversationCount = 0; $conversationCount < self::COUNT; ++$conversationCount) {
            $conversation = new Conversation();
            $em->persist($conversation); // Manage Entity for persistence.
            $team = $faker->randomElement($allTeams);
            $allMembers = $em->getRepository(Member::class)->findByTeam($team);

            //$logger->info('ALL MEMBERS FOR TEAM '.$team->getId(),$allMembers[0]);

            $members = new ArrayCollection($faker->randomElements($allMembers, $count = $faker->numberBetween($min = 2, $max = 5)));
            //$logger->info('MEMBER SELECTION',(array)$members);
            $creationDate = $faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now');
            $conversation
                ->setName($faker->sentence())
                ->setTeam($team)
                ->setMembers($members)
                ->setCreatedAt($creationDate);
            $this->addReference("TestConversation-${conversationCount}", $conversation); // Reference for the next Data Fixture(s).

        }

        $em->flush(); // Persist all managed Entities.

    }
}

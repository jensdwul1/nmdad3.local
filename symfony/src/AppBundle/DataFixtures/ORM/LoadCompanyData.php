<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Company;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadCompanyData.
 *
 */
class LoadCompanyData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerTrait, PasswordTrait;

    const COUNT = 40;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 2; // The order in which fixture(s) will be loaded.
    }

    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        // Fake companys.
        for ($companyCount = 0; $companyCount < self::COUNT; ++$companyCount) {
            $company = new Company();
            $em->persist($company); // Manage Entity for persistence.

            $company
                ->setName($faker->company())
                ->setEmail($faker->companyEmail())
                ->setPhone($faker->phoneNumber())
                ->setCreatedAt($faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now'));
            $this->addReference("TestCompany-${companyCount}", $company); // Reference for the next Data Fixture(s).
        }

        $em->flush(); // Persist all managed Entities.
    }
}

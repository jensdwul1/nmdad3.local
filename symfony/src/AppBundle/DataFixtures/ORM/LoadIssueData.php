<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Issue;
use AppBundle\Entity\Team;
use AppBundle\Entity\Member;
use AppBundle\Entity\Task;
use AppBundle\Entity\Project;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadIssueData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 */
class LoadIssueData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerTrait, PasswordTrait;

    const COUNT = 30;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 11; // The order in which fixture(s) will be loaded.
    }

    public function load(ObjectManager $em)
    {
        $logger = $this->getContainer()->get('logger');

        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $allTeams = $em->getRepository(Team::class)->findAll();

        // Fake issues.
        for ($issueCount = 0; $issueCount < self::COUNT; ++$issueCount) {
            $issue = new Issue();
            $em->persist($issue); // Manage Entity for persistence.
            $team = $faker->randomElement($allTeams);
            $allMembers = $em->getRepository(Member::class)->findByTeam($team);

            //$logger->info('ALL MEMBERS FOR TEAM '.$team->getId(),$allMembers[0]);

            $members = new ArrayCollection($faker->randomElements($allMembers, $count = $faker->numberBetween($min = 1, $max = 3)));
            //$logger->info('MEMBER SELECTION',(array)$members);
            $allProjects = $em->getRepository(Project::class)->findByTeam($team);
            $project = $faker->randomElement($allProjects);
            $creationDate = $faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now');
            $issue
                ->setSubject($faker->sentence())
                ->setBody($faker->paragraph())
                ->setSolved(0)
                ->setProject($project)
                ->setMembers($members)
                ->setSendAt($creationDate)
                ->setCreatedAt($creationDate);
            $this->addReference("TestIssue-${issueCount}", $issue); // Reference for the next Data Fixture(s).

        }

        $em->flush(); // Persist all managed Entities.

    }
}

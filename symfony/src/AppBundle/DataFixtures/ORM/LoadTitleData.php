<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Title;
use AppBundle\Entity\Team;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadTitleData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 */
class LoadTitleData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerTrait, PasswordTrait;

    const COUNT = 40;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 4; // The order in which fixture(s) will be loaded.
    }

    public function load(ObjectManager $em)
    {

        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $allTeams = $em->getRepository(Team::class)->findAll();

        foreach($allTeams as $key => $team){

            //Developer
            $title = new Title();
            $em->persist($title); // Manage Entity for persistence.

            $title
                ->setName('Developer')
                ->setTeam($team)
                ->setCreatedAt($faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now'));
            $this->addReference("TestTitle-Dev-${key}", $title); // Reference for the next Data Fixture(s).

            //Designer
            $title = new Title();
            $em->persist($title); // Manage Entity for persistence.

            $title
                ->setName('Designer')
                ->setTeam($team)
                ->setCreatedAt($faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now'));
            $this->addReference("TestTitle-Des-${key}", $title); // Reference for the next Data Fixture(s).

            //Q&A
            $title = new Title();
            $em->persist($title); // Manage Entity for persistence.

            $title
                ->setName('Q&A')
                ->setTeam($team)
                ->setCreatedAt($faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now'));
            $this->addReference("TestTitle-Q-${key}", $title); // Reference for the next Data Fixture(s).


        }

        $em->flush(); // Persist all managed Entities.

    }
}

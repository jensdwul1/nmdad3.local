<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Team;
use AppBundle\Entity\Account;
use AppBundle\Entity\Company;
use AppBundle\Entity\Member;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadTeamData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 */
class LoadTeamData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerTrait, PasswordTrait;

    const COUNT = 10;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 3; // The order in which fixture(s) will be loaded.
    }

    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $allCompanies = $em->getRepository(Company::class)->findAll();
        $allAccounts = $em->getRepository(Account::class)->findAll();


        // Fake teams.
        for ($teamCount = 0; $teamCount < self::COUNT; ++$teamCount) {
            $team = new Team();
            $em->persist($team); // Manage Entity for persistence.
            $company = $faker->randomElement($allCompanies);
            $team
                ->setName($faker->word())
                ->setIcon($faker->word())
                ->setCompany($company)
                ->setCreatedAt($faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now'));
            $this->addReference("TestTeam-${teamCount}", $team); // Reference for the next Data Fixture(s).

            $member = new Member();
            $em->persist($member); // Manage Entity for persistence.
            $account = $faker->randomElement($allAccounts);
            $member
                ->setIsLeader(1)
                ->setIsEnabled(1)
                ->setIsActive(0)
                ->setTeam($team)
                ->setAccount($account)
                ->setCreatedAt($faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now'));
            $this->addReference("TestTeamLeader-${teamCount}", $member); // Reference for the next Data Fixture(s).

        }

        $em->flush(); // Persist all managed Entities.
    }
}

<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Account;
use AppBundle\Entity\Picture;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadPictureData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 */
class LoadPictureData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerTrait, PasswordTrait;

    const COUNT = 100;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 10; // The order in which fixture(s) will be loaded.
    }

    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $allAccounts = $em->getRepository(Account::class)->findAll();
        foreach($allAccounts as $key => $usr){
            $picture = new Picture();
            $em->persist($picture); // Manage Entity for persistence.
            $dir = '/web/assets/img/accounts/'.$usr->getId().'/';
            $width = 400;
            $height = 400;
            //$image = $faker->image($dir, $width, $height, 'animals', true, true, $faker->word());
            $image = $faker->imageUrl($width, $height, 'animals', true, $faker->word(), true);
            $picture
                ->setSource($image)
                ->setAlternateText($faker->word())
                ->setAccount($usr)
                ->setCreatedAt($faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now'));
            $this->addReference("TestPicture-${key}", $picture); // Reference for the next Data Fixture(s).

        }

        $em->flush(); // Persist all managed Entities.
    }
}

<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Account;
use AppBundle\Entity\Admin;
use AppBundle\Entity\SuperAdmin;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadAccountData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 */
class LoadAccountData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerTrait, PasswordTrait;

    const COUNT = 100;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1; // The order in which fixture(s) will be loaded.
    }

    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        // Admin to log in with manually.
        $admin = new Admin();
        $em->persist($admin); // Manage Entity for persistence.

        $dir = '/web/assets/images/admins/ADMIN ID/';
        $admin
            ->setFirstName('NMDAD-III')
            ->setLastName('Admin')
            ->setUsername('admin')
            ->setUsernameCanonical('admin')
            ->setEnabled(true)
            ->setEmail('admin@pomo.be')
            ->setEmailCanonical('admin@pomo.be')
            ->setBirthday(new DateTime($faker->date($format = 'Y-m-d', $max = '-18 years')))
            ->setRoles(['ROLE_ADMIN'])
            ->setPasswordRaw('admin');
        $this->hashPassword($admin);
        $this->addReference('TestAdmin', $admin); // Reference for the next Data Fixture(s).


        // Account to log in with manually.
        $account = new Account();
        $em->persist($account); // Manage Entity for persistence.

        $width = 400;
        $height = 400;
        $dir = '/web/assets/images/accounts/ACCOUNT ID/';
        $image = $faker->imageUrl($width, $height, 'animals', true, $faker->word(), true);
        $account
            ->setFirstName('NMDAD-III')
            ->setLastName('Test Account')
            ->setUsername('test')
            ->setUsernameCanonical('test')
            ->setEnabled(true)
            ->setAccountPicture($image)
            ->setEmail('test@pomo.be')
            ->setEmailCanonical('test@pomo.be')
            ->setBirthday(new DateTime($faker->date($format = 'Y-m-d', $max = '-18 years')))
            ->setRoles(['ROLE_USER'])
            ->setPasswordRaw('test');
        $this->hashPassword($account);
        $this->addReference('TestAccount', $account); // Reference for the next Data Fixture(s).

        // Fake accounts.
        for ($accountCount = 0; $accountCount < self::COUNT; ++$accountCount) {

            $account = new Account();
            $em->persist($account); // Manage Entity for persistence.

            $width = 400;
            $height = 400;
            $dir = '/web/assets/images/accounts/ACCOUNT ID/';
            $image = $faker->imageUrl($width, $height, 'cats', true, $faker->word(), true);
            $username = $faker->unique()->userName();
            $email = $faker->unique()->email();
            $account
                ->setFirstName($faker->firstName())
                ->setLastName($faker->lastName())
                ->setUsername($username)
                ->setUsernameCanonical($username)
                ->setEnabled(true)
                ->setEmail($email)
                ->setEmailCanonical($email)
                ->setAccountPicture($image)
                ->setRoles(['ROLE_USER'])
                ->setBirthday(new DateTime($faker->date($format = 'Y-m-d', $max = '-18 years')))
                ->setPasswordRaw($faker->password());
            $this->hashPassword($account);
            $this->addReference("TestAccount-${accountCount}", $account); // Reference for the next Data Fixture(s).
        }

        $em->flush(); // Persist all managed Entities.
    }
}

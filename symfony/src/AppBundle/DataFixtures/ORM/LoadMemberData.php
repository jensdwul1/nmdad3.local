<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Account;
use AppBundle\Entity\Title;
use AppBundle\Entity\Team;
use AppBundle\Entity\Member;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadMemberData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 */
class LoadMemberData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerTrait, PasswordTrait;

    const COUNT = 100;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 5; // The order in which fixture(s) will be loaded.
    }

    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $allTeams = $em->getRepository(Team::class)->findAll();
        $allAccounts = $em->getRepository(Account::class)->findAll();
        foreach (range(1, 3) as $i) {
            foreach($allAccounts as $key => $usr){
                $member = new Member();
                $em->persist($member); // Manage Entity for persistence.
                $team = $faker->randomElement($allTeams);
                $allTitles = $em->getRepository(Title::class)->findByTeam($team);
                $title = $faker->randomElement($allTitles);
                $member
                    ->setIsLeader(0)
                    ->setIsEnabled(1)
                    ->setIsActive(0)
                    ->setTeam($team)
                    ->setTitle($title)
                    ->setAccount($usr)
                    ->setCreatedAt($faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now'));
                $this->addReference("TestMember-${i}${key}", $member); // Reference for the next Data Fixture(s).

            }
        }
        $em->flush(); // Persist all managed Entities.
    }
}

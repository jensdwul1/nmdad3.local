<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\TimerSetting;
use AppBundle\Entity\Team;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadTimerSettingData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 */
class LoadTimerSettingData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerTrait, PasswordTrait;

    const COUNT = 40;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 8; // The order in which fixture(s) will be loaded.
    }

    public function load(ObjectManager $em)
    {

        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $allTeams = $em->getRepository(Team::class)->findAll();

        foreach($allTeams as $key => $team){

            //Developer
            $timerSetting = new TimerSetting();
            $em->persist($timerSetting); // Manage Entity for persistence.

            $timerSetting
                ->setDefaultPomo(1500000)
                ->setShortBreak(180000)
                ->setLongBreak(900000)
                ->setCleanUp(4);
            $this->addReference("TestTimerSetting-${key}", $timerSetting); // Reference for the next Data Fixture(s).

        }

        $em->flush(); // Persist all managed Entities.

    }
}

<?php

namespace AppBundle\Traits;

use AppBundle\Entity\User;
use AppBundle\Entity\UserAbstract;

/**
 * Class PasswordTrait.
 */
trait PasswordTrait
{
    /**
     * @param User $user
     */
    public function hashPassword(UserAbstract $user)
    {
        $container = method_exists($this, 'getContainer') ? $this->getContainer() : $this;
        $passwordEncoder = $container->get('security.password_encoder');
        $encodedPassword = $passwordEncoder->encodePassword($user, $user->getPasswordRaw());
        $user->setPassword($encodedPassword);
    }
}

<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/symfony", name="homepage-symfony")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    /**
     * @Route("/", name="homepage")
     * @Template("default/posts.html.twig")
     */
    public function postsAction()
    {
        $em = $this->getDoctrine()->getManager();

//        $posts = $em->getRepository(PostAbstract::class)->findAll();
        $teams = $em->getRepository(Team::class)->findAll(); // Reduces number of queries.

        // Send variables to the view.
        return [
            'teams' => $teams,
        ];
    }
}
